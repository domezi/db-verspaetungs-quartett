<meta charset=utf8>
<script src="vue.global"></script>

<div id="app">
    <div class="navbar">Karten aktuell: {{karten.length +1}} / 110</div>
    <div class="flex-col" style="margin-top:20px">
        <button @click="kategorienSichtbar = !kategorienSichtbar">Kategorien {{kategorienSichtbar ? 'ausblenden' :
            'einblenden'}}
        </button>
        <div class="kategorien flex-col" v-if="kategorienSichtbar">
            <h2>Kategorien</h2>
            <ul>
                <li v-for="kategorie in kategorien">
                    <h2>{{ kategorie }} ({{ karten.filter(karte => karte.kat === kategorie.split(' ')[0]).length }}
                        Karten)</h2>
                    <ul>
                        <li v-for="karte in karten.filter(k => k.kat === kategorie.split(' ')[0])">
                            <h3>{{ karte.titel}}</h3>
                            <p>{{ karte.aktion}}</p>
                        </li>
                    </ul>
                </li>
            </ul>
            <hr>
        </div>
    </div>


    <div class="karten">
        <div v-for="(karte,index) in karten" :class="'karte karte-'+karte.id+' karte-kat-'+karte.kat">
            <div class="vorderseite" :title="index+' (ID='+karte.id+')'">
                <div class="head">
                    <img :src="'play/public/bilder/'+karte.id+'.jpg'" class="bg">
                    <h1>{{karte.titel}}</h1>
                    <div class="kat"> {{karte.kat}}{{karte.kat_char}}</div>
                </div>
                <div class="body">


                    <p>
                        <img src="play/public/speak.png" height="11" style="margin-bottom:-1px"
                             v-if="karte.aktion.includes('🔉') && !karte.ist_zustand">
                        {{karte.aktion.replace('🔉 ','')}}</p>

                    <div :class="{puls:true, best: karte.ist_zustand}"></div>

                    <!-- have statistics for haufigkeit, verspatung, verwirrung, lacherlichkeit -->


                </div>
                <div class="stats-container">
                    <div class="stats">
                        <div class="stat">
                            <div class="best"
                                 v-if="best.haufigkeit === karte.haufigkeit || karte.haufigkeit == '-1'"></div>
                            <div class="best" v-if="karte.haufigkeit == '-1'" style="right:16px"></div>
                            <div class="num">{{karte.haufigkeit}}</div>
                            <div class="description">Häufigkeit</div>
                        </div>
                        <div class="stat">
                            <div class="best" v-if="best.verspatung === karte.verspatung"></div>
                            <div class="num">{{karte.verspatung}}</div>
                            <div class="description">Verspätung</div>
                        </div>
                    </div>
                    <div class="stats">
                        <div class="stat">
                            <div class="best"
                                 v-if="best.verwirrung === karte.verwirrung || karte.verwirrung == '11'"></div>
                            <div class="best" v-if="karte.verwirrung == '11'" style="right:16px"></div>

                            <div class="num">{{karte.verwirrung}}</div>
                            <div class="description">Verwirrung</div>
                        </div>
                        <div class="stat">
                            <div class="best"
                                 v-if="best.lacherlichkeit === karte.lacherlichkeit  ||  karte.lacherlichkeit == '11'"></div>
                            <div class="best" v-if="  karte.lacherlichkeit == '11'" style="right:16px"></div>
                            <div class="num">{{karte.lacherlichkeit}}</div>
                            <div class="description">Lächerlichkeit</div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="karte">
            <div class="regeln">
                <h3>Ablauf</h3>
                <ul>
                    <li>Aktion auf Karte durchlesen.<br>(bei <img src="play/public/speak.png" height="11"
                                                                  style="margin-bottom:-2px;margin-right:2px"> laut
                        vorlesen)
                    </li>
                    <li>Aktion auf Aktionskarten (roter Puls) ausführen.</li>
                    <li>Trumpfen, falls möglich.</li>
                    <li>Bei einem Gewinn mit einer Fähigkeitskarte (grüner Puls)
                        <b>muss</b> diese nun offen als eigene Fähigkeit auf den Tisch gelegt werden und
                        bei nachfolgenden Zügen angewendet werden. Wenn bereits 4 Karten im
                        Fähigkeiten-Bereich liegen, muss die älteste Karte aufgenommen werden.
                    </li>
                </ul>

                <div class="info">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                         stroke="currentColor" class="w-6 h-6">
                        <path stroke-linecap="round" stroke-linejoin="round"
                              d="M11.25 11.25l.041-.02a.75.75 0 011.063.852l-.708 2.836a.75.75 0 001.063.853l.041-.021M21 12a9 9 0 11-18 0 9 9 0 0118 0zm-9-3.75h.008v.008H12V8.25z"/>
                    </svg>
                    <span>
                Solange eine Karte als Fähigkeit auf dem Tisch liegt, zählt sie <b>nicht</b> als Handkarte.
  Geht der Handstapel zur Neige, dürfen Fähigkeitskarten <b>nicht</b> aufgenommen werden!
                </span>
                </div>

                <h3 style="margin-top:10px">
                    Wer gewinnt beim Trumpfen?</h3>
                <ul>
                    <li>Verspätung, Verwirrung, Lächerlichkeit<br>&uarr; größter Wert</li>
                    <li style="margin-top:9px">Häufigkeit<br>&darr; kleinster Wert</li>
                </ul>

                <div class="info" style="display: none">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                         stroke="currentColor" class="w-6 h-6">
                        <path stroke-linecap="round" stroke-linejoin="round"
                              d="M11.25 11.25l.041-.02a.75.75 0 011.063.852l-.708 2.836a.75.75 0 001.063.853l.041-.021M21 12a9 9 0 11-18 0 9 9 0 0118 0zm-9-3.75h.008v.008H12V8.25z"/>
                    </svg>
                    <span>Die besten Werte der Skala haben einen grünen Punkt im Feld.</span>
                </div>
            </div>
        </div>

        <div class="karte" v-if="false">
            <div class="regeln">
                <h3> Durchschnittswerte</h3>
                <ul>
                    <li>Häufigkeit: {{durchschnitt.haufigkeit}}</li>
                    <li>Verspätung: {{durchschnitt.verspatung}}</li>
                    <li>Verwirrung: {{durchschnitt.verwirrung}}</li>
                    <li>Lächerlichkeit: {{durchschnitt.lacherlichkeit}}</li>
                </ul>
                <h3 style="margin-top:10px">Kategorien</h3>
                <ul>
                    <li v-for="kategorie in kategorien">
                        <span class="kategorie">{{kategorie}} ({{karten.filter(k => k.kat === kategorie.split(' ')[0]).length}})</span>
                    </li>
                </ul>
                <h3 style="margin-top:10px">Karten Verhältnis</h3>
                <ul>
                    <li>{{karten.filter(k => k.ist_zustand).length}} Fähigkeiten / {{karten.filter(k =>
                        !k.ist_zustand).length}} Aktionen
                    </li>
                </ul>

            </div>
        </div>
    </div>
</div>

<script>
    const {createApp} = Vue

    const kategorien = `
1 Bahnhöfe
2 Züge
3 Einflüsse von außen
4 Betriebsablauf
5 Bauarbeiten & BOS
6 Sonderereignisse
7 Spezialkarten
8 Tickets
9 Essen
10 Sprechen
11 Sitzplatz
12 Winter
13 Durch die Nacht?
14 Umrangieren
15 Bordtechnik
`
    //

    const csv = `Kartegorie;Grund der Verspätung;Häufigkeit;Verspätung (min);Verwirrung;Lächerlichkeit;Aktion beim ausspielen
0;/;1;Frankfurt Hbf;10;10;4;9;Spucke auf den Boden. Wenn Du in der DB Lounge bist, hole Dir einen Espresso.
1;+;2;ICE 271 › Basel SBB;10;30;0;10;Rege Dich über den ICE Typ 4 und die Verbindung zwischen Offenburg und Freiburg auf.
2;/;3;Beeinträchtigung durch Vandalismus;1;15;2;1;🔉 Wenn Du mehr als 10 Karten hast, klaue Deinem Gegenüber ein paar Karten. Bei Polizeieinsatz klaut Dein Gegenüber Dir ein paar Karten.
3;+;4;Warten auf Anschlussreisende;...3;...10;...4;...2;Wenn ein anderer am Zug ist, warte beim Trumpfen mit Deiner Reaktion, bis er Dich wirklich mehrfach auffordert.
4;--;4;Technische Störungen;6;40;8;10;Drehe Deinen Kartenstapel so, dass die Schrift kopfüber steht. Sobald eine Karte mit grünem Puls aufgedeckt wird, darfst Du die Karten wieder umdrehen.
5;++;4;Kurzfristiger Personalausfall;7;90;8;9;Falls Du auf Toilette musst oder auf irgendetwas anderes Lust hast, mach das jetzt. Deine Mitspielenden müssen so lange warten, bis Du wieder da bist.
6;/;3;Unwetterschäden;6;180;10;1;Vernichte diese Karte. Lege zusätzlich 5 Karten von Deinem Gegenüber aus dem Spiel.
7;+;3;Person im Gleis;4;20;7;2;🔉 Du spielst von nun an im Mittelgang weiter, bis ein Polizeieinsatz oder eine Reparatur kommt.
8;+;3;Tiere im Gleis;4;20;7;0;🔉 Vegane Menschen mit mehr als 5 Karten auf der Hand gewinnen in dieser Runde sofort.
9;+;5;Reparatur am Zug;-;-;-;-;🔉 Du darfst alle Deine Karten anschauen und neu sortieren. Allerdings hast Du dafür nur 5 Sekunden Zeit. Mische diese Karte im Anschluss nach hinten.
10;++;4;Außergewöhnlich hohe Auslastung;6;10;5;8;Halbiere Deinen Stapel. Du hast ab jetzt auf beiden Händen ein Deck Karten und spielst immer abwechselnd linkes Deck, rechtes Deck.
11;/;4;Gleiswechsel;3;5;9;5;🔉 Tja, jetzt ist die andere Person am Zug.
12;+;1;Frankfurt Flughafen;7;5;11;6;🔉 Hier ist Verwirrung gegeben, da reicht die 10er Skala nicht aus! Dein Gegenüber muss übrigens seine Karten mischen.
13;--;4;Zugausfall;3;0;10;4;🔉 Dein nächster Zug fällt aus, wortwörtlich! Dein Gegenüber darf Dich zweimal hintereinander herausfordern.
14;++;4;SEV;6;60;10;9;Gewinnst Du den Stich nicht, musst Du diese Karte möglichst umständlich zum Gewinner befördern. Es reicht nicht, sie einfach in die Hand zu geben.
15;++;6;Streikauswirkungen;2;120;6;1;Falls Du diese Karte in einer Runde hergeben sollst, erstreite Dir eine alternative Einigung mit dem Gewinner. Dieser muss nicht kooperativ sein.
16;-;4;Anschluss vsl. nicht erreichbar;5;100;8;8;🔉 Du verpasst nicht nur Deinen Anschluss, sondern auch den Zug danach… Verliert sofort gegen Frankfurt Hbf/Flughafen oder ICE 271.
17;+;4;Verspätung aus vorheriger Fahrt;4;20+?;7;7;Du darfst immer die Verspätung Deiner letzten Karte addieren. Nimm diese Karte bei "Warten auf Anschlussreisende" wieder auf.;1
18;++;5;Polizeieinsatz;3;30;6;9;🔉 Du bestimmst, wer alle seine Fähigkeitskarten wieder aufnehmen muss.
19;++;5;Reparatur an der Strecke;6;40;8;7;🔉 Sollte eure Spielunterlage schräg sein, darfst Du sie jetzt wieder gerade machen. Auch doppelte Kartenstapel werden wieder zusammengelegt.
20;/;4;Störung an einem anderen Zug;3;30;9;9;Lege diese Karte jetzt zur Seite und lege Sie zu einem beliebigen Zeitpunkt Deiner Wahl wieder ganz oben auf.;1
21;++;5;Umleitung wegen Streckensperrung;5;70;8;8;🔉 Wechselt alle den Spielort, es sei denn, ein anderer hat "Reparatur an der Strecke" als oberste Karte.
22;/;4;Verspätete Bereitstellung;2;50;6;10;🔉 Schaue Dir Deine nächsten drei Karten an. Du darfst dabei auch die Reihenfolge dieser Karten ändern.
23;/;5;Feuerwehreinsatz auf der Strecke;2;40;5;3;🔉 Personen, die auf dem Gang spielen, dürfen wieder zu ihren Plätzen zurückkehren.
24;++;5;Beschädigung einer Brücke;1;20;10;7;🔉 Kippt eure Spielunterlage so, dass Karten beim Ablegen herrunterrutschen. Wird durch "Bauarbeiten" oder "Reparatur an der Strecke" aufgehoben.
25;/;5;Defektes Stellwerk;4;15;4;3;Mische Deinen Kartenstapel nach dieser Runde einmal ordentlich durch, dass nichts mehr ist, wo es hingehört.
26;++;5;Bauarbeiten;4;20;9;5;🔉 Wenn Du jetzt am Zug bist, stelle eure Spielunterlage wieder gerade.
27;/;4;Verspätung im Ausland;2;80;3;4;🔉 Du gewinnst sofort gegen Frankfurt Hbf/Flughafen oder ICE 271. Schließlich wirst Du genug Zeit haben, Deinen Anschlusszug zu erreichen.
28;++;7;Wünsch dir was;?;?;?;?;Du darfst Dir Werte und Aktion auf dieser Karte ausdenken. Merken Deine Mitspielenden, dass Du diese Karte hast, verlierst Du in dieser Runde.
29;++;6;Zug ist Pünktlich;0;0;10;0;Dies ist nur ein theoretisches Konstrukt und in der Praxis nicht relevant.
30;++;8;Bahncard 100;3;45;0;0;🔉 Dein Wort ist Gesetz. Eine Regel oder Aktion, die Du jetzt verordnest, bleibt den Rest des Spieles erhalten. Alle müssen ohne Zweifel danach handeln.
71;++;8;Probe Bahncard 100;3;45;0;0;🔉 Du darfst Dir eine Regel ausdenken. Diese gilt aber nur für die nächsten 3 Runden.
31;/;10;WIFI@DB;5;0;9;10;Mob les Int rnet gehö t in D utschl nd zur sel|enen Sp zies. Ve ringert dei e Hœufigkeit imm r um 5.;1
32;+;7;Fahrgastrechte Formular;8;120;8;8;Bestehe auf Deine Rechte! Wer die Runde gewinnt, legt diese Karte zur Seite. Sie kann dann als Aktions-Abwehr (auch die eigener Karten) genutzt werden.;1
33;++;7;Am Service-Schalter hinten anstellen;3;75;7;5;Wer die Runde gewinnt, legt diese Karte zur Seite. Sie kann genutzt werden, um eine Aktion für eine Runde zu verzögern.;1
34;++;15;Verstopfte Toiletten;5;0;4;7;Da fühlt sich die Verspätung noch viel länger an! Erhöht Deine Verspätung bis zur nächsten Reparatur um 30 Minuten.;1
35;++;6;Zug ist überpünktlich;-1;-1;10;-1;Dies ist nur ein theoretisches Konstrukt und in der Praxis nicht relevant.
36;+;8;Bahncard 25;10;45;0;5;🔉 Wer hat sie nicht? Wenn Du am Zug bist, gib sie direkt an Deinen Gegenüber.
37;+;8;Bahncard 25;10;45;0;5;🔉 Wer hat sie nicht? Wenn Du am Zug bist, gib sie direkt an Deinen Gegenüber.
38;+;8;Bahncard 25;10;45;0;5;🔉 Wer hat sie nicht? Wenn Du am Zug bist, gib sie direkt an Deinen Gegenüber.
39;++;8;Die Fahrtkarten bitte!;7;0;2;0;🔉 Entfernt alle jeweils eine Karte aus dem Spiel. Wer keine Bahncard 25 im Stapel hat, verliert 2 Karten.
40;++;11;Sitzplatzreservierung;7;0;6;9;🔉 "Ich glaube Sie sitzen da auf meinem Platz!" - Du darfst Sitzplatz und Karten mit einer anderen Person tauschen, wenn Du möchtest.;1
41;++;8;Keine Fahrkarte;7;0;5;0;Verlierst Du diese Karte, darf niemand bemerken, dass Du sie hattest, bis sie im gegnerischen Kartenstapel ist. Nimm ansonsten Deine Fähigkeitskarten wieder auf.
42;++;14;Umgedrehte Wagen-Reihung;5;5;9;2;🔉 Die Reihung der Wagen ist umgedreht. Die schlechtesten Werte sind jetzt die besten und umgekehrt. Die Aktionen bleiben erhalten.
43;/;5;Umbau der DB Lounge;4;0;5;8;Da fühlt sich die Verspätung noch viel länger an! Verdoppelt Deine Verspätung bis zur nächsten Reparatur.;1
44;++;14;Zugtrennung;4;0;7;2;🔉 Entferne die Hälfte von den Karten Deines Gegenüber und diese Karte aus dem Spiel.
45;++;8;9€ Ticket;8;60;7;0;Was für ein Durcheinander! Jeder Spieler muss jetzt seinen Kartenstapel mischen. Diese Karte erhöht Deine Verwirrung immer um 1.;1
46;+;4;In den falschen Zug einsteigen;2;?;?;6;Deine Verspätung ist 5x die Anzahl der Karten von Deinem Gegenüber. Und die Verwirrung weisste vermutlich am besten selbst!!
47;+;4;Fahrradmitnahme;2;15;5;5;Hups, da ist ja noch ein fremdes Fahrrad im Zug! Nimm eine bei einem anderen ausliegende Fähigkeitskarte, und lege Sie bei Dir aus.
48;++;8;Fahrkarte;9;10;1;0;🔉 Lasse diese Karte durch eine:n Fahrgastbegleiter:in abstempeln, und sie kann zur Abwehr von Bahncard 100 Regeln sowie der Ticket-Kontrolle genutzt werden.;1
49;+;8;Gruppen-Ticket;4;0;6;3;🔉 Finde eine Begleitung. Muss jemand von euch eine Karte abgeben, muss die andere Person das auch tun.;1
50;++;8;1. Klasse Aufpreis;1;0;0;8;🔉 Während diese Karte vor Ihnen liegt, werden Sie von allen mit "Sie" angesprochen. Wer Sie dutzt, muss Ihnen für jedes "Du" eine Karte geben.;1
51;++;8;Geklaute Fahrkarte;9;10;1;0;🔉 Du tauscht mit einer anderen Person die Namen. Wer euch falsch anspricht, muss euch eine Karte geben.
52;++;6;Zeitumstellung;1;-60;10;6;🔉 Eure Kartenstapel werden einmal entgegen dem Uhrzeigersinn getauscht.
53;++;9;Speisekarte;5;120;0;0;🔉 Solange die Speisekarte vor Dir liegt, bist Du durchgängig am futtern und schlemmern. Bediene Dich gern auch ab und zu an gegnerischen Karten.;1
<<<<<<< HEAD
54;++;10;Sänk ju;8;0;7;9;🔉 You must speak in se bahn english in se next rounds until se next trein stetion or lokel trein.
55;+;1;Haltepunkt Dammtor;4;0;9;7;🔉 An diesem Bahnhof ... - äh Haltepunkt ist schon so mancher verloren gegangen. Gib alle Bahnhöfe an die anderen ab und entferne die Karte aus dem Spiel.
=======
54;++;10;Sänk ju;8;0;7;9;🔉 You mascht schpeak in se bahn english in se next rounds until se next trein steschen or lokel trein.
55;+;1;Haltepunkt Dammtor;4;0;9;7;🔉 An diesem Bahnhof ... - äh Haltepunkt ist schon so mancher verloren gegangen. Gib alle Bahnhöfe an die anderen ab und entferne diese Karte aus dem Spiel.
>>>>>>> 4957c8bbdd34e14b554a38cec571bc0218fce1d3
56;+;1;Wolfsburg Hbf;-;-;-;-;Das Bielefeld der Bahn - Vergesse diese Runde einfach zu spielen. Pack die Karte nach hinten und Dein Gegenüber ist am Zug.
57;+;14;Kupplung;7;15;2;0;🔉 Doppelte Kartenstapel werden wieder vereint.
58;+;14;Doppeltraktion;5;0;6;0;🔉 Du darfst beim Trumpfen immer zwischen Deinen oberen beiden Karten wählen, welche Du spielen möchtest.;1
59;+;14;Kopfbahnhof;3;0;6;3;🔉 Du darfst beim Trumpfen immer zwischen Deiner oberen und der hinterstern Karte wählen, welche Du spielen möchtest.;1
60;++;15;SiFa;10;0;3;5;🔉 Du musst die anderen alle 60s daran erinnern, dass sie diese Karte drücken. Wird sie nicht gedrückt, erhälst Du +60min Verspätung im nächsten Zug.;1
61;-;10;Der Sitznachbar;8;0;4;6;Erzähle eine wahrhaftige Geschichte über Deine:n alte:n Bekannte:n (eine fremde Person im Zug).
62;++;10;Kundendialog;3;60;7;8;🔉 Du darfst niemanden mehr direkt ansprechen, sondern musst immer über einen Dritten kommunizieren. Erhöht Deine Verwirrung um 3.;1
63;++;7;Klebrige Finger;6;0;5;8;Erhöht Deine Lächerlichkeit um 2, Du darfst aber nur noch zwei Finger benutzen. Tust Du das nicht, musst Du Schmierfink jedem Spieler eine Karte geben.;1
64;+;15;Funkloch;10;0;0;10;🔉 Sprich für diese Runde nur jedes zweite Wort Deines Satzes, so wie Du es aus dem deutschen Mobilfunknetz kennst.
65;++;15;Lautstärkeregler;2;0;6;4;🔉 Du darfst die Sprechlautstärke eines Mitspielenden einstellen. Dies gilt bis zur nächsten Ticket-Kontrolle oder Polizeieinsatz.
66;++;10;Durchsage am Bahnsteig;7;15;7;2;Sprich ab jetzt komplett unverständlich. Erhöht Deine Verwirrung um 2.;1
67;++;15;Durchsage im Zug;9;0;8;6;Sprich in bestem Bahn-Deutsch. Natürlich mit guter Begrüßung und Verabschiedung. Erhöht Deine Lächerlichkeit um 2.;1
68;+;1;Stuttgart 21;9;60/30;10;9;Erhöht die Verspätung um 60 Minuten, wenn Du Bauarbeiten im Stapel hast. Falls nicht, halbiert sich Deine Verspätung.;
69;+;9;Gin Tonic;3;0;8;0;Strafkarten bei Regelverstoß entfallen für Dich, da Du nicht zurechnungsfähig bist. Wirst Du aber erwischt, musst Du diese Karte wieder aufnehmen.;1
70;-;7;BWegt;10;30;0;11;Verdoppelt Deine Verspätung und Deine Lächerlichkeit. Halbiert aber zeitgleich Deine Häufigkeit und Deine Verwirrung.;1
72;+;10;Schweigeabteil;7;0;7;3;🔉 Diese Runde dürft Ihr nur noch mit Zeichen kommunizieren. Wer das nicht tut, muss eine Karte abgeben.
73;+;6;Überfüllter Mülleimer;7;10;6;4;🔉 Nimm bis zu 5 Karten auf, die zuvor aus dem Spiel entfernt wurden. Entferne dafür diese Karte aus dem Spiel.
74;+;6;Junggesellenabschied;2;25;7;5;🔉 Was für ein nerviges Durcheinander! Nimm alle ausliegenden Fähigkeitskarten, mische sie und teile sie im Uhrzeigersinn neu aus.
75;+;10;Defekte Ansage;2;0;10;6;🔉 Lese beim Lautsprecher Symbol nur die Überschrift vor. Die Aktionen werden als bekannt vorausgesetzt. Bei Regelverstoß erhälst Du die Strafkarten.;1
76;--;15;Falsche Anzeige im ICE Display;4;0;10;7;🔉 Eure Zahlen gehören diese Runde zu anderen Werten. Häufigkeit <=> Lächerlichkeit Verwirrung <=> Verspätung werden getauscht.
77;--;15;Klimaanlage kaputt;9;30;2;9;🔉 Oh Nein! Ziehe zwei Kleidungsstücke aus oder an. Wenn Du nicht genügend Kleidungsstücke hast, findest Du bestimmt jemand, der Dir hilft.
78;+;4;Zugüberfüllung;8;30;2;4;🔉 Addiert beim Trumpfen jeweils die Werte von euren nächsten beiden Karten.
79;+;7;Aktuelle Alternative;6;35;9;8;Du darfst vor dem Trumpfen eine Fähigkeitskarte als aktuelle Karte aufnehmen, riskierst aber natürlich sie zu verlieren.;1
80;--;7;Unbeaufsichtigtes Gepäckstück;2;20;9;2;🔉 Lege den Rest Deines Kartenstapels zur Seite. Es könnte aber passieren, dass Dir Karten weggenommen werden, wenn Du nicht drauf achtest!;1
81;--;15;Türstörung;7;10;8;7;🔉 Führt alle Aktionen aus, die Spiel Karten verbleiben aber jeweils bei euch. Ihr müsst also nicht trumpfen.
82;+;7;Bahnbonus App;5;0;5;5;Wer hat, dem wird gegeben. Für je 10 Karten auf der Hand darfst Du vom Stapel der zuvor aus dem Spiel gelegten Karten eine Karte der Wahl wieder aufnehmen.
83;+;4;Verfahren;1;60;10;8;Wenn Du diese Runde verlierst, kannst Du noch einmal mit einem anderen Wert trumpfen.;1
84;+;7;Familienabteil;4;0;2;4;🔉 Ha, ein Kinderspiel! Lass Deine Karten auf den Boden fallen. Hebe sie dann in einer von Dir gewählten Reihenfolge wieder auf.
85;--;6;Notbremse;1;100;0;0;🔉 Wenn es eins gibt was wie am Schnürchen läuft, dann bis Du es in dieser Runde. Du läufst nämlich den Rest von der Strecke und kommst 45 Minuten später.;1
86;+;5;Neubaustrecke;4;30;6;6;🔉 Mal geht sie, mal geht sie nicht. Wenn Du am Zug bist, verringert sich Deine Verspätung um 30 Minuten, ansonsten erhöht sie sich um 30 Minuten.;1
87;+;7;Deutschlandtakt;6;20;2;2;🔉 Deine Mitspielenden haben immer eine feste Verspätung von 20 Minuten.;1
88;+;9;Die Apfeltasche;2;0;0;0;Die gute Apfeltasche! Leider immer nur in Düsseldorf, Köln oder Frankfurt zu haben, und das auch nicht immer. Verringert Deine Häufigkeit immer um 2.;1
89;+;9;Vegane Currywurst;3;0;2;4;🔉 Oh nein, das Wurst-Case Szenario ist eingetreten! Alle mit ausliegenden Fähigkeitskarten müssen eine davon aus dem Spiel entfernen.
90;+;9;Kaffeeautomat;2;15;0;0;Diese Karte ist selten gut. Der Kaffe macht einen klaren Kopf und verringert Deine Verwirrung um 2. Die 30 Minuten Kaffepause kommst Du aber später.;1
91;--;5;Maskenkontrolle;4;10;0;4;🔉 Der unbestechliche Sicherheitsdienst der Bahn! Kommt es aufgrund gleicher Wertigkeit zum Stich, so gewinnst Du sofort.;1
92;/;15;Defekte Steckdose;2;0;3;3;🔉 Mein Akku ist fast alle! Sofort trumpfen, keine Aktionen vorlesen oder ausführen.
93;+;6;0 min Umstiegszeit;1;0;8;0;Jetzt bloß keine Verspätung, alles andere ist egal! Ihr trumpft in dieser Runde um die Verspätung, sofern die anderen Karten dem nicht widersprechen.
94;++;7;Rittersport Werbung;7;0;0;9;Nimm alle Fähigkeitskarten auf die Hand, lege dafür bis zu zwei beliebige Fähigkeitskarten wieder aus. Praktisch und quadratisch halt!
95;--;9;Defekter Getränkeautomat;4;0;3;0; Nein! Das Getränk hängt fest! Schlage so lange auf den Tisch bis eine Karte herunterfällt. Du darfst diese Karte dann auf die Hand nehmen.
96;+;9;Das Notfall Wasser;3;90;0;10;🔉 Wir entschuldigen uns für die Unannehmlichkeiten. Dürfen wir Ihnen etwas anbieten? Bedienen Sie sich! Ziehe von allen Mitspielenden eine Karte.
97;/;10;West- und Ostsignalsystem;4;0;8;9;🔉 Ersetze beim Sprechen die Worte Häufigkeit, Verspätung, Verwirrung und Lächerlichkeit. Dafür darfst Du überall 1 addieren (Umrechnungsfaktor).;1
98;+;15;ETCS;3;15;5;4;🔉 Jemand anderes soll vorlesen. Lege Dich entspannt zurück und Deinen Stapel offen hin. Zum trumpfen wird automatisch Dein bester Wert genommen.
99;++;9;Hartgekochte Eier;8;0;2;7;🔉 Puuuuh, das riecht man wirklich! Alle müssen sich 1m entfernt von Dir hinsetzen.;1
100;+;8;49€ Ticket;5;10;8;5;🔉 Ziemlich teuer! Legt alle die oberen beiden Karten in die Mitte. Ihr trumpft nun mit der nächsten Karte um diesen Kartenstapel ohne ihre Aktion anzuwenden.
101;--;11;Bahnsteigkante;8;5;3;2;🔉 Rutsche so, dass Du in 1m Entfernung vom Spielbereich sitzt. Oder halt soviel wie geht. Bei Umleitung, Bauarbeiten oder Reparatur darfst Du zurück.
102;+;12;Verschwundener Bahnhof;3;20;8;1;🔉 Nur noch Schnee zu sehen! Alle müssen sich ihre Werte einprägen und dann für diese Runde mit verdecktem Kartendeck spielen.
103;-;12;Gefrorene Laterne;4;0;4;1;🔉 Macht das Licht aus. Erhöht Deine Verwirrung um 2. Falls Du kein Licht ausmachen kannst, entferne diese Karte nach dem trumpfen aus dem Spiel.;1
104;+;12;Eingeschränkte Sicht;3;20;5;2;🔉 Wenn Du eine Brille hast, lege sie ab. Wenn Du keine hast, setze die eines Mitspielenden auf. Erhöht Deine Verwirrung und Lächerlichkeit jeweils um 1.;1
105;+;12;Eingeschneite Treppe;2;30;7;4;🔉 Ne, oder? Da rutscht man auf der letzten Stufe aus und fällt wieder die ganze Treppe nach unten. Macht die komplette letzte Runde wieder rückgängig.
106;+;12;Unzugängliche Tür;3;10;7;4;🔉 Ein paar Türen funktionieren nicht. Du darfst in dieser Runde wählen, welche Aktionskarten ihre Wirkung erzielen, und welche nicht in Kraft treten.
107;+;12;Verschneite Fahrgastinformation;5;15;5;5;🔉 Was ich nicht weiss, macht mich nicht heiß. Keine Aktion wird ausgeführt. Du gewinnst, wenn Du korrekt vorhergesagt hast, ob Du gewinnst oder verlierst.
108;+;12;Zugefrorene Weiche;4;30;3;2;🔉 Dann müsst ihr wohl auf dem gleichen Gleis bleiben. Trumpft mit der selben Kategorie wie im letzen Zug.
109;--;12;Traumlandschaft;1;0;0;0;🔉 Es gewinnt in dieser Runde die Karte mit dem schöneren Bild. Bei Unentschiedenheit behalten alle ihre Karten.
110;+;12;Schnee-Ersatzverkehr;3;20;6;3;🔉 Wenn jemand eine Winter-Karte oben auf hat, darfst Du verlangen, dass diese gegen die nächste Nicht-Winter Karte im Stapel ersetzt wird.
111;++;12;Einsamer Bahnsteig;3;45;4;1;🔉 Hier ziehts! Und ich vertrag doch keinen Zug. Setze bis zum nächsten Bahnhof aus, solange es noch mehr als 2 Spieler:innen gibt.
112;+;1;Kiel Hbf;1;0;0;0;Schönster Bahnhof in Deutschland. Gewinnt einfach gegen alle Bahnhöfe.
113;++;12;Unlesbares Schild;2;10;7;3;🔉 Ohje, alle Karten auf dem Tisch sind eingeschneit! Für diese Runde verlieren Fähigkeitskarten ihre Wirkung.
114;+;12;In den Bergen;4;10;0;0;🔉 Der Berg der Höllentalbahn braucht viel Power, und bei so einer Schräge entstehen auch mal schiefe Zahlen. Multipliziere alle Werte mit Faktor 1,5714.;1
115;+;12;Schnee von Gestern;4;0;5;3;🔉 Wer in der letzen Runde gewonnen hat, gewinnt diese Runde wieder. Ihr braucht also gar nicht erst zu trumpfen.;1
116;/;12;Weg vom Fenster;4;5;6;7;Tja, jetzt hast Du verloren. Ne, Spass. Aber wende Dich für diese Runde dem Spiel ab und schaue aus dem Fenster. Du darfst natürlich blind mitspielen.;1
117;++;12;Schneegional Zug;3;0;0;8;Ist sehr schneell, daher ist die Verspätung immer null. Er ist aber ziemlich witzig, deswegen ist Deine Lächerlichkeit immer um 3 erhöht.;1
118;--;12;Gleiskalt;3;0;4;6;🔉 Ey soo kalt hier. Vermeide unnötige Bewegungen in dieser Runde. Du gibst keine Karte ab, kannst aber welche erhalten, wenn Du gewinnst.
119;+;2;Regenbogen ICE;2;0;5;0;Was soll der Geiz mit den Farben? Warum nicht Bunter leben? Du darfst eine erfolgreich verteidigte Aktionskarte als Fähigkeitskarte vor Dir ablegen.;1
120;+;12;In den Schnee gefallen;5;15;8;1;🔉 Spiele Hütchenspiel gegen Deinen Gegenüber mit Deinen drei oberen Karten. Wenn er diese Karte nicht beim ersten Versuch findet, behalte sie.
121;++;12;Rodelbahn;5;-;>9000;-;🔉 Bist Du im falschen Spiel? Hier geht es um Eisenbahnen! Lege diese Karte aus dem Spiel.;1
122;+;6;Kaputte Oberleitung;3;60;5;0;🔉 Vergiss es, der Tag is gelaufen. Es gelten in dieser Runde nur die negativen Auswirkungen aller Fähigkeitskarten.
123;/;2;Dampflokomotive;2;0;2;0;🔉 Früher gabs den ganzen modernen Schnick Schnack nicht. Spielt diese Runde ohne Beachtung der Fähigkeitskarten oder anderer Aktionskarten.
124;+;5;Defekter Pantograph;3;45;2;2;🔉 Du hast heute keinen guten Draht nach oben. Gewinnst Du einen Stich, wirf eine Münze. Bei Zahl bekommt die Karten Dein Gegenüber, sonst Du.;1
125;-;1;Hundertwasser Bahnhof in Uelzen;2;5;10;2;Wer will schon diese langweiligen Standardfarben? Alle Karten dieser Runde mit den DB Rot/Weiss Farben im Bild werden aus dem Spiel entfernt.
126;+;1;Offenburg Bhf;9;45;8;8;🔉 In der Nacht ist es hier sehr gefährlich! Wenn es draußen gerade dunkel ist, darf Dein Gegenüber Dir eine Karte seiner Wahl abnehmen.
127;--;5;Maskenmuffel;4;30;4;6;Kommt es aufgrund gleicher Wertigkeit zum Stich, so verlierst Du sofort. Verliert gegen Maskenkontrolle, erhöht aber Deine Lächerlichkeit um 3.;1
128;+;7;Tag der offenen Tür;3;10;5;6;🔉 Ein Fest für neugierige Nasen! Spiele diese Fähigkeitskarte irgendwann aus, und Du darfst Dir die oberste Karte Deines Gegenüber anschauen.
129;++;9;Bienenstich;3;0;5;5;🔉 Der leckerste Kuchen aller Zeit! Kommt es aufgrund gleicher Wertigkeit zum Stich, so gewinnst Du sofort.;1
130;+;11;Ggf. reserviert;7;0;8;6;🔉 Heute keine Sitzplatzreservierung. Sucht euch reihum einen neuen Sitzplatz aus. (Inklusive aller Karten);1
131;--;6;Fussball Fans;3;15;0;9;🔉 Einmal und nie wieder! Setzt Deine Häufigkeit auf 1 wenn Du in der Defensive bist.;1
132;++;11;Nackenhörnchen;4;0;3;10;🔉 Diese Runde geht auf Deinen Nacken. Du gibst jedem eine Karte Deiner Wahl.
133;/;2;Nacht Zug;2;0;5;7;🔉 Einmal und nie wieder! Reduziert Deine Häufigkeit auf maximal 1 wenn Du in der Defensive bist.;1
134;--;15;ICE Portal;9;0;2;6;🔉 Sag auswendig einen Bahn Fakt, den niemand weiß. Dieser Fakt wird anschließen überprüft und entscheidet über den Gewinner dieser Runde.
135;+;7;Trainspotter;3;0;0;8;🔉 Alle, die einen Zug auf der Karte abgebildet haben, zeigen dir ihre Karte. Tausche diese Karte vor dem Trumpfen gegen eine der gezeigten Karten aus.
136;+;14;Prellbock;3;0;5;5;🔉 Wer gegen Dich trumpft, muss Dich in zwei Kategorien besiegen. Bei der zweiten Kategorie gelten jedoch Deine anderen Fähigkeitskarten nicht.;1
137;++;4;Disponent:in;7;0;8;6;🔉 Karten müssen ab jetzt immer über Dich an die Gewinner weitergegeben werden. Bahnhöfe, die Du so bekommst, musst Du nicht weitergeben.;1
138;/;9;Freigetränk;4;0;7;4;🔉 Besitzt Dein Gegenüber eine oder mehrere Getränkekarten, bekommst Du sie.
139;+;12;Bahnbrechende Geschwindigkeit;5;-15;4;2;🔉 Reduziert Deine Verspätung um 15min. Du gewinnst allerdings sofort gegen alle Züge. Nimm diese Karte bei der nächsten Reparatur wieder auf.;1
140;+;7;Der Puls;10;0;0;0;🔉 Das Wahrzeichen der Bahn. Ein geheimnisvolles Objekt, nach dem alle wie verrückt sind. Der meistbietende dieser Runde darf diese Karte sein eigen nennen.
141;++;2;Güterzug;4;10;2;2;Egal wie gut Dein Zug ist, mein Zug ist Güter! Gewinnt in dieser Runde gegen alle anderen Züge.
142;--;7;DB Verspätungs Quartett;10;60;8;8;🔉 Wem das Spiel gehört, der steht auch über dem Spiel. Darf sich etwas wünschen, das ausschließlich Effekt außerhalb dieses Spiels hat.
143;+;12;Polar Express;2;0;4;4;🔉 Du darfst bei Karten der Winter-Edition immer einen Wert Deiner Wahl um eine Einheit verbessern, wenn Du diese Karte vor Dir liegen hast. ;1
144;++;9;Ingwertiertes Spiel;4;0;9;7;Hat Ingwer nach neuen Regeln gefragt? Hier sind sie. Immer, wenn Du verlierst, gewinnst Du und umgekehrt.;1
145;+;7;Zugucken;4;0;4;6;Richtig gehört. Du darfst ab jetzt nur noch zugucken. Sobald Du einen Regelverstoß siehst, darfst Du ihn aber melden und ab dann wieder mitspielen.
146;++;2;TrainLab;4;0;4;6;🔉 Innovation bringt den Fortschritt. Erhöht daher Dein Fähigkeitskarten Limit auf 6 Karten.;1
147;--;2;Verärgerter Fahrgast;4;0;4;6;Sobald die Person mit einem Personalausfall nicht anwesend ist, darfst Du beliebig Karten aus Deinem Stapel mit denen der anderen Person austauschen.
`

    createApp({
        data() {
            return {
                karten: [],
                best: {
                    haufigkeit: "0",
                    verspatung: "180",
                    verwirrung: "10",
                    lacherlichkeit: "10",
                },
                kategorien: kategorien.trim().split("\n"),
                kategorienSichtbar: false,
            }
        },
        mounted: function () {

            let rows = csv.split('\n')

            // remove first row
            rows.shift()
            rows.pop()

            // remove empty rows
            rows = rows.filter(row => row.length > 0)

            // give them IDs
            /*
            rows = rows.map((row, index) => {
                return index + ';' + row
            })
             */

            // remove Lorem Ipsum
            // rows = rows.filter(row => !row.includes('Lorem Ipsum'))

            // remove lines starting with a number followed by ;--;
            rows = rows.filter(row => row.match(/^\d+;\+/))


            let kat_count = []

            rows = rows.map(row => {
                const [id, rating, kat, titel, haufigkeit, verspatung, verwirrung, lacherlichkeit, aktion, ist_zustand] = row.split(';')
                if (!kat_count[kat]) {
                    kat_count[kat] = 0
                }

                kat_count[kat]++
                if (String.fromCharCode(64 + parseInt(kat_count[kat])) == 'I' || String.fromCharCode(64 + parseInt(kat_count[kat])) == 'O') {
                    kat_count[kat]++
                }

                return {
                    rating,
                    kat,
                    kat_char: String.fromCharCode(64 + parseInt(kat_count[kat])),
                    kat_title: this.kategorien[kat - 1].split(' ')[1],
                    id,
                    titel,
                    haufigkeit,
                    verspatung,
                    verwirrung,
                    lacherlichkeit,
                    aktion,
                    ist_zustand
                }
            })

            // store
            this.karten = rows

        },
        computed: {
            durchschnitt: function () {
                let durchschnitt = {
                    haufigkeit: 0,
                    verspatung: 0,
                    verwirrung: 0,
                    lacherlichkeit: 0,
                }

                let durchschnitt_count = {
                    haufigkeit: 0,
                    verspatung: 0,
                    verwirrung: 0,
                    lacherlichkeit: 0,
                }


                this.karten.forEach(karte => {
                    /* if they are numbers */
                    if (!isNaN(parseInt(karte.haufigkeit))) {
                        durchschnitt.haufigkeit += parseInt(karte.haufigkeit)
                        durchschnitt_count.haufigkeit++
                    }
                    if (!isNaN(parseInt(karte.verspatung))) {
                        durchschnitt.verspatung += parseInt(karte.verspatung)
                        durchschnitt_count.verspatung++
                    }
                    if (!isNaN(parseInt(karte.verwirrung))) {
                        durchschnitt.verwirrung += parseInt(karte.verwirrung)
                        durchschnitt_count.verwirrung++
                    }
                    if (!isNaN(parseInt(karte.lacherlichkeit))) {
                        durchschnitt.lacherlichkeit += parseInt(karte.lacherlichkeit)
                        durchschnitt_count.lacherlichkeit++
                    }
                })

                durchschnitt.haufigkeit = durchschnitt.haufigkeit / durchschnitt_count.haufigkeit
                durchschnitt.verspatung = durchschnitt.verspatung / durchschnitt_count.verspatung
                durchschnitt.verwirrung = durchschnitt.verwirrung / durchschnitt_count.verwirrung
                durchschnitt.lacherlichkeit = durchschnitt.lacherlichkeit / durchschnitt_count.lacherlichkeit

                return durchschnitt
            }
        }
    }).mount('#app')
</script>

<link rel="stylesheet" href="index.css">

